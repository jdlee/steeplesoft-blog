<!DOCTYPE html>
<html>
    <head>
        <title>Coming Up for Air<#if content.title?? && content.title?has_content>: ${content.title}</#if></title>


        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="alternate" type="application/rss+xml" title="jasondl.ee RSS feed" href="/feed.atom">
        <!--
        <link rel="stylesheet" type="text/css" href="/styles/asciidoctor-default.css">
        <link rel="stylesheet" type="text/css" href="/styles/coderay-asciidoctor.css">
        -->

        <!-- Bootstrap -->
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- Local overrides, etc -->
        <link rel="stylesheet" type="text/css" href="/styles/lightbox/lightbox.css">
        <link rel="stylesheet" type="text/css" href="/styles/style.css">
        <link rel="stylesheet" type="text/css" href="/styles/custom.css">
        <!-- Web Fonts -->
        <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Cherry+Cream+Soda">
        <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Philosopher">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css">
        <style type="text/css"></style>
        <!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
        <script src="/scripts/lightbox/jquery-1.7.2.min.js" type="text/javascript"></script>
        <script src="/scripts/lightbox/lightbox.js" type="text/javascript"></script>
        <script src="/scripts/foundation/foundation.js" type="text/javascript"></script>
    </head>
    <body style="padding-left: 10px; padding-right: 10px;">
        <#include "misc.ftl">
        <div class="row">
            <div class="blogname">
                <span>
                    <a href="/">Coming Up for Air</a>
                </span>
            </div>
        </div>
        <div class="row main">
            <div class="col-md-9 columns">
                <div id="content">
