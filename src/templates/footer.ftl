                        </div>
                    </div>
                    <div class="col-md-3 columns widget">
                        <div class="columns">
                            <h3>My Links
                            </h3>
                            <ul>
                                <li>
                                    <a href="/about.html" title="About">About</a>
                                </li>
                                <li>
                                    <a href="/presentations" title="Presentations">Presentations</a>
                                </li>
                                <li>
                                    <a href="resume.docx" rel="me" title="Resume/CV">Resume/CV</a>
                                </li>
                                <li>
                                    <a href="https://linkedin.com/in/jasondlee" rel="me" title="LinkedIn">LinkedIn</a>
                                </li>
                                <li>
                                    <a href="https://twitter.com/jasondlee" rel="me" title="Twitter">Twitter</a>
                                </li>
								<!--
                                <li>
                                    <a href="/projects.html" title="Projects">Projects</a>
                                </li>
								-->
                            </ul>

                        </div>
                        <div class="columns">
                            <h3>Quotes</h3>
                            <p id="quote">Sample quote</p>
                            <p id="source">Quote source</p>
                        </div>
                        <div class="columns">
                            <h3>Publications</h3>
                            <ul>
                                <li>
                                    <table width="100%" style="border: 0">
                                    <tr>
                                        <td>
									        <p style="text-weight: bold; font-size: large">
										        Java 9 Programming Blueprints:
										        <a title="My Java 9 book at Amazon" href="http://amzn.to/2FD2XAo" style="padding-left: 10px">Amazon</a>
										        <a title="My Java 9 book at Packt" href="https://www.packtpub.com/application-development/java-9-programming-blueprints" style="padding-left: 10px">Packt</a>
									        </p>
									    </td>
                                        <td>
									        <a href="http://amzn.to/2FD2XAo"><img src="/images/2017/j9pb.png" width="100"/></a>
                                        </td>
                                    </tr>
                                    </table>

                                </li>
                                <li>
                                    <!--<a title="This article discusses ways to use Java EE specs to implement a simple, yet powerful plugin system with a minimum of thrid party libraries. " href="http://jaxenter.com/java-tech-journal/JTJ-2012-06">Tips for Writing Pluggable Java EE Applications</a>-->
                                    <a title="This article discusses ways to use Java EE specs to implement a simple, yet powerful plugin system with a minimum of thrid party libraries. " href="https://jaxenter.com/tips-for-writing-pluggable-java-ee-applications-105281.html">Tips for Writing Pluggable Java EE Applications</a>
                                </li>
                                <li>
                                    <a title="An article Ken Paulsen and I wrote detailing easy JSF component creation" href="http://www.oracle.com/technetwork/articles/javaee/jsf-templating-137822.html">JSFTemplating and Woodstock: Component Authoring Made Easy</a>
                                </li>
                                <li>
                                    <a href="http://jsfcentral.com/articles/lee-03-09.html">Jason Lee in depth: Mojarra and Scales</a>
                                </li>
                                <li>
                                    <!--<a title="My JavaOne 2008 Wrap Up" href="http://www.theserverside.com/news/thread.tss?thread_id=49343">Jason Lee: Postmortem for JavaOne 2008</a>-->
                                    <a title="My JavaOne 2008 Wrap Up" href="https://web.archive.org/web/20090301060746/http://www.theserverside.com/news/thread.tss?thread_id=49343">Jason Lee: Postmortem for JavaOne 2008</a>
                                </li>
                                <li>
                                    <!--<a title="The GlassFish Adoption Story from my time at IEC" href="https://blogs.oracle.com/stories/entry/international_environmental">International Environmental: A Cooling Company Which Prefers Hot Software</a>-->
                                    <a title="The GlassFish Adoption Story from my time at IEC" href="https://web.archive.org/web/20130621192312/https://blogs.oracle.com/stories/entry/international_environmental">International Environmental: A Cooling Company Which Prefers Hot Software</a>
                                </li>
                                <li>
                                    <a title="This details the project that spawned a couple of the Mojarra Scales components" href="http://www.jsfcentral.com/articles/trenches_7.html">IEC donates custom JSF component to Mojarra Scales</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            <div class="footer">
                <div class="small-12 columns">
                    <p>&#169; Copyright Jason Lee, 2005-2020. All rights reserved.</p>
                </div>
            </div>
            <script>
                (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
                ga('create', 'UA-1091049-6', 'auto');
                ga('send', 'pageview');
            </script>
            <script src="/scripts/quotes.js" type="text/javascript"></script>
            <script src="/scripts/local.js" type="text/javascript"></script>
            <script src="https://cdn.jsdelivr.net/gh/google/code-prettify@master/loader/run_prettify.js?lang=css&amp;skin=sunburst"></script>
        </div>
    </body>
</html>
